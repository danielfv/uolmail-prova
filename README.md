# README #

Avaliação UOL Mail

# SHELL #

1) Para o arquivo “Input” utilize comandos bash para:

Identificar o usuário que tem o maior "size";
```
#!bash

sort -nk 5 input | tail -n 1
```

Ordenar o “username” em ordem alfabética.
```
#!bash

sort -k 1 input
```

Identificar a quantidade de usuários que estão na seguinte faixa do INBOX:

* Entre 25000 e 48999 msgs

```
#!bash

awk -F' ' '$3 >= 25000 && $3 <= 48999' input | wc -l
```

* Entre 49000 e 50000 msgs

```
#!bash

awk -F' ' '$3 >= 49000 && $3 <= 50000' input | wc -l
```

* Acima de 50001 msgs

```
#!bash

awk -F' ' '$3 >= 50001' input | wc -l
```


# API #
### O que tem aqui? ###

* Escopo
* Códigos fontes
* Manual de execução
* Exemplos
* Demonstração

### Escopo ###

Utilizando o mesmo arquivo, desenvolva uma API que possibilite:

* Fazer busca por usuário
* Identificar o usuário que tem a maior quantidade de “INBOX”
* Identificar o usuário que tem o maior “size”
* Listar todos os usuários, com o backend suportando paginação.

Desenvolva uma interface simples para utilizar a API implementada, nesta interface é esperado que tenha um campo de busca compatível com os métodos da API além de listar todos os usuários

Implemente alguns testes unitários e end-to-end.

Para a implementação da API, interface e testes pode utilizar qualquer linguagem/tecnologia. 

Hospedar o sistema desenvolvido em algum lugar (gratuito)

Enviar o código para a consultoria ou colocá-lo em algum repositório que seja possível acessa-lo. 


### Requisitos ###
1. Java 1.8
2. Maven (Opcional para teste)

### Manual de execução ###

1. Realizar download do projeto: https://bitbucket.org/danielfv/uolmail-prova/downloads
2. Descompactar o projeto
3. Executar o server

```
#!bash

java -jar uolmail-jar-with-dependencies.jar 
```

## Teste ##
Implemente alguns testes unitários e end-to-end.

```
#!bash

mvn test
```

## Demonstração ##

http://54.152.197.137:8080/

### Padrões ###

* Paginação: 30 itens
* Ordenação: emails

## Exemplos ##

### Api ###

* Exibir email: http://localhost:8080/emails/mavaxmedu@uol.com.br
* Lista de emails: http://localhost:8080/emails
* Paginação: http://localhost:8080/emails?page=10
* Paginação e tamanho da lista: http://localhost:8080/emails?page=10&size=100
* Maior INBOX: http://localhost:8080/emails?field=messages
* Maior SIZE: http://localhost:8080/emails?field=size

### Web ###

* Interface: http://localhost:8080