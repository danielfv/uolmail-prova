package br.com.uol.prova.servlets;

import java.io.IOException;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import br.com.uol.prova.servlets.annotations.Attribute;
import br.com.uol.prova.servlets.annotations.Jsp;

public abstract class JspServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	private String jsp;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.request = request;
		this.response = response;

		doRequest();

		Class<?> type = this.getClass();

		if (jsp == null) {
			if (type.isAnnotationPresent(Jsp.class))
				jsp = type.getAnnotation(Jsp.class).value();
		}

		for (Method method : type.getDeclaredMethods()) {
			if (!method.isAnnotationPresent(Attribute.class))
				continue;

			Attribute attribute = method.getAnnotation(Attribute.class);

			try {
				method.setAccessible(true);
				Object o = method.invoke(this);
				request.setAttribute(attribute.value(), o);
			} catch (Exception ex) {

			}
		}

		if (jsp != null)
			request.getRequestDispatcher(jsp).forward(request, response);
	}

	public String getJsp() {
		return jsp;
	}

	public void setJsp(String jsp) {
		this.jsp = jsp;
	}

	public abstract void doRequest() throws ServletException, IOException;

	public void writeJsonObject(Object object) throws IOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(object);

		response.setContentLength(json.length());
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		response.getOutputStream().write(json.getBytes());
		response.flushBuffer();
	}
}
