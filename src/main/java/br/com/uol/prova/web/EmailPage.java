package br.com.uol.prova.web;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import javax.servlet.annotation.WebServlet;
import com.google.gson.Gson;
import br.com.uol.prova.dao.InputPageable;
import br.com.uol.prova.servlets.JspServlet;
import br.com.uol.prova.servlets.annotations.Attribute;
import br.com.uol.prova.servlets.annotations.Jsp;

@WebServlet("/")
@Jsp("/email.jsp")
public class EmailPage extends JspServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doRequest() throws IOException {
	}

	@Attribute("inputs")
	public InputPageable search() throws IOException {
		String email = request.getParameter("query");
		String page = request.getParameter("page");
		String size = request.getParameter("size");

		String query = "?";
		if (email != null) {
			query = "/" + email;
		} else {
			if (page != null)
				query += "page=" + page;

			if (size != null) {
				if (!query.equals("?"))
					query += "&";

				query += "size=" + size;
			}
		}

		return getInputPageableByUrl("http://localhost:8080/emails" + query);
	}

	@Attribute("inputByMaxMessage")
	public InputPageable getByMaxMessage() throws IOException {
		return getInputPageableByUrl("http://localhost:8080/emails?order=asc&field=messages");
	}

	@Attribute("inputByMaxSize")
	public InputPageable getByMaxSize() throws IOException {
		return getInputPageableByUrl("http://localhost:8080/emails?order=asc&field=size");
	}

	private InputPageable getInputPageableByUrl(String sUrl) throws IOException {
		URL url = new URL(sUrl);
		Reader reader = new InputStreamReader(url.openStream());
		Gson gson = new Gson();
		return gson.fromJson(reader, InputPageable.class);
	}
}
