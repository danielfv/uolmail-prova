package br.com.uol.prova.web.rest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import javax.servlet.annotation.WebServlet;
import br.com.uol.prova.dao.Input;
import br.com.uol.prova.dao.InputDAO;
import br.com.uol.prova.dao.InputPageable;
import br.com.uol.prova.servlets.JspServlet;

@WebServlet(urlPatterns = "/emails/*")
public class ApiRest extends JspServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doRequest() throws IOException {
		InputDAO inputDAO = InputDAO.getInstance();

		String field = request.getParameter("field");
		String email = request.getPathInfo();

		if (email != null) {
			if (email.startsWith("/"))
				email = email.substring(1);

			if (email.trim().isEmpty())
				email = null;
		}

		if (email != null) {
			if (email.startsWith("/"))
				email = email.substring(1);

			super.writeJsonObject(new InputPageable(inputDAO.get(email.trim())));
		} else if (field != null) {
			super.writeJsonObject(new InputPageable(inputDAO.max(field)));
		} else {
			String sSize = request.getParameter("size");
			if (sSize == null)
				sSize = "30";

			Integer size = Integer.parseInt("0" + sSize.replaceAll("[^0-9]", ""));
			if (size <= 0)
				size = 1;

			String sPage = request.getParameter("page");
			if (sPage == null)
				sPage = "1";

			Integer page = Integer.parseInt("0" + sPage.replaceAll("[^0-9]", ""));
			page--;

			Collection<Input> inputs = inputDAO.list(field);
			Integer fullSize = inputs.size();

			int start = page * size;
			if (start >= inputs.size())
				start = inputs.size();

			int end = page * size + size;

			if (end >= inputs.size())
				end = inputs.size();

			inputs = Arrays.asList(inputs.toArray(new Input[]{})).subList(start, end);
			super.writeJsonObject(new InputPageable(inputs, page + 1, size, fullSize));
		}
	}
}
