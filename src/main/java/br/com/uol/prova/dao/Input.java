package br.com.uol.prova.dao;

public class Input implements Comparable<Input> {
	private String email;
	private Integer messages;
	private Integer size;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getMessages() {
		return messages;
	}

	public void setMessages(Integer messages) {
		this.messages = messages;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return email;
	}

	@Override
	public int compareTo(Input o) {
		return email.compareTo(o.getEmail());
	}
}
