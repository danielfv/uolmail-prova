package br.com.uol.prova.dao;

import java.util.ArrayList;
import java.util.Collection;

public class InputPageable {
	private Integer size = 0;
	private Integer fullSize = 0;
	private Integer page = 1;

	private String next;
	private String previous;
	private Collection<Input> result;

	public InputPageable(Input input) {
		result = new ArrayList<Input>();
		if (input != null) {
			result.add(input);
			size = 1;
			fullSize = 1;
		}
	}

	public InputPageable(Collection<Input> result, Integer page, Integer pageSize, Integer fullSize) {
		this.result = result;
		this.page = page;
		size = result.size();
		this.fullSize = fullSize;

		if (pageSize <= result.size())
			next = "?page=" + (page + 1);

		if (page - 1 > 0)
			previous = "?page=" + (page - 1);

		if (size == 0)
			previous = "?page=" + (fullSize / pageSize + 1);
	}

	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getFullSize() {
		return fullSize;
	}
	public void setFullSize(Integer fullSize) {
		this.fullSize = fullSize;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}
	public Collection<Input> getResult() {
		return result;
	}
	public void setResult(Collection<Input> result) {
		this.result = result;
	}
}
