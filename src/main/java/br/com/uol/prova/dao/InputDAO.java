package br.com.uol.prova.dao;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class InputDAO {
	private static InputDAO instance = null;
	private String filename;
	private SortedMap<String, Input> sortedInputMap;

	public static InputDAO getInstance() throws IOException {
		if (InputDAO.instance != null) 
			return InputDAO.instance;
		
		String context = "";

		InputDAO.instance = new InputDAO();
		InputDAO.instance.setFilename(new File(context).getAbsolutePath() + "/input");
		return InputDAO.instance;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) throws IOException {
		this.filename = filename;
		sortedInputMap = loadFile();
	}

	private SortedMap<String, Input> loadFile() throws IOException {
		InputStream inputStream = new FileInputStream(getFilename());
		byte[] b = new byte[4096];
		int i = 0;

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		while ((i = inputStream.read(b)) != -1) {
			baos.write(b, 0, i);
		}
		
		baos.flush();
		inputStream.close();
		
		SortedMap<String, Input> map = new TreeMap<>();

		String text = new String(baos.toByteArray());
		for (String row : text.split("\n")) {
			String[] col = row.split(" ");

			Input input = new Input();
			input.setEmail(col[0].trim());
			input.setMessages(Integer.parseInt(col[2]));
			input.setSize(Integer.parseInt(col[4]));

			map.put(input.getEmail(), input);
		}

		return map;
	}

	public Input get(String email) {
		return sortedInputMap.get(email);
	}

	public Collection<Input> list() {
		return sortedInputMap.values();
	}

	public Collection<Input> list(String field) {
		return sort(field);
	}

	public Input max(String field) {
		return sort(field).last();
	}

	public SortedSet<Input> sort(String field) {
		Comparator<Input> comparator = null;

		if (field == null)
			field = "email";

		field = field.trim();

		if (field.equalsIgnoreCase("messages")) {
			comparator = new Comparator<Input>() {
				@Override
				public int compare(Input o1, Input o2) {
					return o1.getMessages().compareTo(o2.getMessages());
				}
			};
		} else if (field.equalsIgnoreCase("size")) {
			comparator = new Comparator<Input>() {
				@Override
				public int compare(Input o1, Input o2) {
					return o1.getSize().compareTo(o2.getSize());
				}
			};
		} else {
			comparator = new Comparator<Input>() {
				@Override
				public int compare(Input o1, Input o2) {
					return o1.getEmail().compareTo(o2.getEmail());
				}
			};
		}

		SortedSet<Input> sortedSet = new TreeSet<>(comparator);
		sortedSet.addAll(sortedInputMap.values());
		return sortedSet;
	}
}
