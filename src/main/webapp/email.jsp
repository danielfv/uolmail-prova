<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE>
<html>
<head>
<title>Prova - Uol Mmail</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#" class="navbar-brand"><img height="30" style="margin-top: -5px" src="http://imguol.com.br/p/email/login/uol/img/logo-bg-white.png"></a>
			</div>

			<c:if test="${param['query'] == null}">
				<form class="navbar-form navbar-left">
					<div class="row">
						<div class="col-xs-8">
							<input type="email" name="query" class="form-control" placeholder="email@uol.com.br">
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn btn-default">Pesquisar</button>
						</div>
					</div>
				</form>
			</c:if>

			<c:if test="${param['query'] != null}">
				<form class="navbar-form navbar-left">
					<a href="/" class="btn btn-danger">Limpar</a>
				</form>
			</c:if>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Email</th>
							<th>Mensagens</th>
							<th>Tamanho</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="input" items="${inputs.result}" varStatus="status">
							<tr>
								<th scope="row">${status.index + 1}</th>
								<td>${input.email}</td>
								<td>${input.messages}</td>
								<td>${input.size}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<nav aria-label="...">
					<ul class="pager">
						<c:if test="${inputs.previous != null}">
							<li class="previous"><a href="${inputs.previous}"><span aria-hidden="true">&larr;</span> Anterior</a></li>
						</c:if>
						<c:if test="${inputs.next != null}">
							<li class="next"><a href="${inputs.next}">Pr�ximo <span aria-hidden="true">&rarr;</span></a></li>
						</c:if>
					</ul>
				</nav>
			</div>
			<div class="col-sm-5">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Maior INBOX</h3>
					</div>
					<div class="panel-body form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<p class="form-control-static">${inputByMaxMessage.result[0].email}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mensagens</label>
							<div class="col-sm-9">
								<p class="form-control-static">${inputByMaxMessage.result[0].messages}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Tamanho</label>
							<div class="col-sm-9">
								<p class="form-control-static">${inputByMaxMessage.result[0].size}</p>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Maior SIZE</h3>
					</div>
					<div class="panel-body form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<p class="form-control-static">${inputByMaxSize.result[0].email}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Mensagens</label>
							<div class="col-sm-9">
								<p class="form-control-static">${inputByMaxSize.result[0].messages}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Tamanho</label>
							<div class="col-sm-9">
								<p class="form-control-static">${inputByMaxSize.result[0].size}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>