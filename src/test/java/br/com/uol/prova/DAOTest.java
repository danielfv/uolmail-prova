package br.com.uol.prova;

import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import br.com.uol.prova.dao.InputDAO;

public class DAOTest {
	@Test
	public void loadInstace() {
		try {
			InputDAO iDao = InputDAO.getInstance();
			Assert.assertNotNull(iDao.getFilename());
		} catch (IOException e) {

			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void getMaxInbox() {
		try {
			InputDAO iDao = InputDAO.getInstance();
			Assert.assertNotNull(iDao.max("messages"));
		} catch (IOException e) {

			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void getMaxSize() {
		try {
			InputDAO iDao = InputDAO.getInstance();
			Assert.assertNotNull(iDao.max("size"));
		} catch (IOException e) {

			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void list() {
		try {
			InputDAO iDao = InputDAO.getInstance();
			Assert.assertNotNull(iDao.list());
		} catch (IOException e) {

			Assert.fail(e.getMessage());
		}
	}
}
