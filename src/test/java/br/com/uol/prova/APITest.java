package br.com.uol.prova;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import org.junit.Assert;
import org.junit.Test;
import com.google.gson.Gson;
import br.com.uol.prova.dao.InputPageable;

public class APITest {
	@Test
	public void apiMaxMessages() {
		try {
			InputPageable ip = this.getInputPageableByUrl("http://localhost:8080/emails?order=asc&field=messages");
			Assert.assertTrue(ip.getResult().size() > 0);
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void apiMaxSize() {
		try {
			InputPageable ip = this.getInputPageableByUrl("http://localhost:8080/emails?order=asc&field=size");
			Assert.assertTrue(ip.getResult().size() > 0);
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void apiFindEmail() {
		try {
			InputPageable ip = this.getInputPageableByUrl("http://localhost:8080/emails/bumizci@uol.com.br");
			Assert.assertTrue(ip.getResult().size() > 0);
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void apiPage() {
		try {
			InputPageable ip = this.getInputPageableByUrl("http://localhost:8080/emails?page=10");
			Assert.assertTrue(ip.getResult().size() > 0);
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
	}

	private InputPageable getInputPageableByUrl(String sUrl) throws IOException {
		URL url = new URL(sUrl);
		Reader reader = new InputStreamReader(url.openStream());
		Gson gson = new Gson();
		return gson.fromJson(reader, InputPageable.class);
	}
}
